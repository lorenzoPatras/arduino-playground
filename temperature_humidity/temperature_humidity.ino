// DHT11 is a sensor that measures temperature and humidity
#include <SimpleDHT.h>

#define PIN_DHT11 2

SimpleDHT11 dht11;

void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.println("=================================");
  Serial.println("Data from DHT11 sensor...");
  
  // read with raw sample data
  byte temperature = 0;
  byte humidity = 0;
  byte data[40] = {0};
  
  if (dht11.read(PIN_DHT11, &temperature, &humidity, data)) { // read function returns 0 upon success
    Serial.print("Read DHT11 failed");
    return;
  }
  
  Serial.print("RAW Bits: ");
  for (int i = 0; i < 40; i++) {
    Serial.print((int)data[i]);
    if (i > 0 && ((i + 1) % 4) == 0) {
      Serial.print(' ');
    }
  }
  Serial.println("");
  
  Serial.print("Data: ");
  Serial.print((int)temperature); Serial.print(" *C, ");
  Serial.print((int)humidity); Serial.println(" %");
  
  // DHT11 sampling rate is 1HZ.
  delay(1000);
}
