// uses a 74HC595 module

#define DELAY_MS 100
#define PIN_LATCH 11      // (11) ST_CP [RCK] on 74HC595
#define PIN_CLK 9      // (9) SH_CP [SCK] on 74HC595
#define PIN_DATA 12     // (12) DS [S1] on 74HC595

byte leds = 0;

void updateShiftRegister() {
   digitalWrite(PIN_LATCH, LOW);
   shiftOut(PIN_DATA, PIN_CLK, LSBFIRST, leds);
   digitalWrite(PIN_LATCH, HIGH);
}

void setup() {
  pinMode(PIN_LATCH, OUTPUT);
  pinMode(PIN_DATA, OUTPUT);  
  pinMode(PIN_CLK, OUTPUT);
}

void loop() {
  leds = 0;
  updateShiftRegister();
  delay(DELAY_MS);
  
  for (int i = 0; i < 8; i++) {
    bitSet(leds, i);
    updateShiftRegister();
    delay(DELAY_MS);
  }
}
