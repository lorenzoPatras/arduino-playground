#include "HCSR04.h"
#define TRIG_PIN 12
#define ECHO_PIN 11
UltraSonicDistanceSensor sr04 = UltraSonicDistanceSensor(TRIG_PIN, ECHO_PIN);
long a;

void setup() {
   Serial.begin(9600);
   delay(1000);
}

void loop() {
   a=sr04.measureDistanceCm();
   Serial.print(a);
   Serial.println("cm");
   delay(1000);
}
