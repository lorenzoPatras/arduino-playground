// define pins
int LED = 5;
int BUTTON_ON = 9;
int BUTTON_OFF = 8;

void setup() {
  //set the LED as OUTPUT
  pinMode(LED, OUTPUT);

  //set both button pins as INPUT_PULLUP
  pinMode(BUTTON_ON, INPUT_PULLUP);  
  pinMode(BUTTON_OFF, INPUT_PULLUP);  
}

void loop() {
  //if the "ON" button is pressed, turn ON the led
  if (digitalRead(BUTTON_ON) == LOW) {
    digitalWrite(LED, HIGH);
  }
  
  //if the "OFF" button is pressed, turn OFF the led
  if (digitalRead(BUTTON_OFF) == LOW) {
    digitalWrite(LED, LOW);
  }
}
